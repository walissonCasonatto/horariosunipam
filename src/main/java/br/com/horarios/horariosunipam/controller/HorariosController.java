package br.com.horarios.horariosunipam.controller;

import br.com.horarios.horariosunipam.restBeans.HorariosBean;
import br.com.horarios.horariosunipam.restEntities.Horario;
import br.com.horarios.horariosunipam.restEntities.Laboratorio;
import br.com.horarios.horariosunipam.viewModel.HorariosViewModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@ViewScoped
public class HorariosController implements Serializable{
    @EJB
    private HorariosBean horariosBean;
    private List<HorariosViewModel> horarios;
    private List<Laboratorio> laboratorios;
    private List<Integer> diaSemana;
    private Integer labSelecionado;
    private Integer diaSelecionado;
    
    
    @PostConstruct
    public void init(){
        FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        horarios = horariosBean.getHorarios();
        laboratorios = horariosBean.getLabs();
        diaSemana = new ArrayList<Integer>();
        diaSelecionado = 0;
        labSelecionado=0;
        
    }
    public void atualizaLista(){
        horarios = horariosBean.getHorarios(labSelecionado,diaSelecionado);
    }

    public HorariosBean getHorariosBean() {
        return horariosBean;
    }

    public void setHorariosBean(HorariosBean horariosBean) {
        this.horariosBean = horariosBean;
    }

    public List<HorariosViewModel> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<HorariosViewModel> horarios) {
        this.horarios = horarios;
    }

    public List<Laboratorio> getLaboratorios() {
        return laboratorios;
    }

    public void setLaboratorios(List<Laboratorio> laboratorios) {
        this.laboratorios = laboratorios;
    }

    public List<Integer> getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(List<Integer> diaSemana) {
        this.diaSemana = diaSemana;
    }

    public Integer getLabSelecionado() {
        return labSelecionado;
    }

    public void setLabSelecionado(Integer labSelecionado) {
        this.labSelecionado = labSelecionado;
    }

    public Integer getDiaSelecionado() {
        return diaSelecionado;
    }

    public void setDiaSelecionado(Integer diaSelecionado) {
        this.diaSelecionado = diaSelecionado;
    }
    
    

    
    
}
