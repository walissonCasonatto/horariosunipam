
package br.com.horarios.horariosunipam.restEntities;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Laboratorio {
    private int id;
    private String lab;

    public Laboratorio() {
        id = 0;
        lab = "";
    }

    public Laboratorio(int id, String lab) {
        this.id = id;
        try {
            byte[] bytes = lab.getBytes("UTF-8");
            this.lab = new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            this.lab = lab;
            Logger.getLogger(Laboratorio.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLab() {
        return lab;
    }

    public void setLab(String lab) {
        this.lab = lab;
    }

    @Override
    public String toString() {
        return lab;
    }

    

}
