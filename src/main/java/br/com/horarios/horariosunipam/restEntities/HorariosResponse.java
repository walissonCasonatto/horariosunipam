package br.com.horarios.horariosunipam.restEntities;

import java.util.List;

public class HorariosResponse {

    private List<Horario> horarios;

    public HorariosResponse() {
    }

    public HorariosResponse(List<Horario> horarios) {
        this.horarios = horarios;
    }

    public List<Horario> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<Horario> horarios) {
        this.horarios = horarios;
    }
    
    
}