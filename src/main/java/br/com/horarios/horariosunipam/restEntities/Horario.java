package br.com.horarios.horariosunipam.restEntities;

public class Horario {
    private String nomeLab;
    private String nomeAula;
    private String diaSemana;
    private String periodo;
    private String nomeProf;
    private String nomeTurma;

    public Horario() {
    }

    public Horario(String nomeLab, String nomeAula, String diaSemana, String periodo, String nomeProf, String nomeTurma) {
        this.nomeLab = nomeLab;
        this.nomeAula = nomeAula;
        this.diaSemana = diaSemana;
        this.periodo = periodo;
        this.nomeProf = nomeProf;
        this.nomeTurma = nomeTurma;
    }

    public String getNomeLab() {
        return nomeLab;
    }

    public void setNomeLab(String nomeLab) {
        this.nomeLab = nomeLab;
    }

    public String getNomeAula() {
        return nomeAula;
    }

    public void setNomeAula(String nomeAula) {
        this.nomeAula = nomeAula;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getNomeProf() {
        return nomeProf;
    }

    public void setNomeProf(String nomeProf) {
        this.nomeProf = nomeProf;
    }

    public String getNomeTurma() {
        return nomeTurma;
    }

    public void setNomeTurma(String nomeTurma) {
        this.nomeTurma = nomeTurma;
    }
    

}
