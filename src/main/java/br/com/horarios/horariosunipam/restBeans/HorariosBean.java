package br.com.horarios.horariosunipam.restBeans;

import br.com.horarios.horariosunipam.restClient.HorariosClient;
import br.com.horarios.horariosunipam.restEntities.Horario;
import br.com.horarios.horariosunipam.restEntities.Laboratorio;
import br.com.horarios.horariosunipam.viewModel.HorariosViewModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
@Stateless
public class HorariosBean implements Serializable{
    @Inject
    private HorariosClient horariosService;

    
    
    
    public List<HorariosViewModel> getHorarios(){
        return converteHorariosEmViewModel(horariosService.getHorarios());
    }
    public List<Laboratorio> getLabs(){
        return horariosService.getLabs();
    }
    
    
    public HorariosClient getHorariosService() {
        return horariosService;
    }

    public void setHorariosService(HorariosClient horariosService) {
        this.horariosService = horariosService;
    }

    public List<HorariosViewModel> getHorarios(Integer labSelecionado, Integer diaSelecionado) {
        try {
            if(labSelecionado >0 && diaSelecionado > 0){
                return converteHorariosEmViewModel(horariosService.getHorarios(labSelecionado,diaSelecionado));
            }else if(labSelecionado >0){
                return converteHorariosEmViewModel(horariosService.getHorariosLab(labSelecionado));
            }else if(diaSelecionado>0){
                return converteHorariosEmViewModel(horariosService.getHorariosDia(diaSelecionado));
            }else{
                return converteHorariosEmViewModel(horariosService.getHorarios());
            }
            
        } catch (Exception e) {
            Logger.getLogger(HorariosBean.class.getName()).log(Level.SEVERE,e.getMessage());    
            return new ArrayList<HorariosViewModel>();
            
        }
        
    }
    private List<HorariosViewModel> converteHorariosEmViewModel(List<Horario> lista){
        List<HorariosViewModel> horarios = new ArrayList<HorariosViewModel>();
        for (Horario h : lista) {
            horarios.add(new HorariosViewModel(h));
            
        }
        return horarios;
    }
    
    
}
