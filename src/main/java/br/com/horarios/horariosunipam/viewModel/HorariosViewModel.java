package br.com.horarios.horariosunipam.viewModel;

import br.com.horarios.horariosunipam.restEntities.Horario;

public class HorariosViewModel {
    public String lab;
    public String periodo;
    public boolean vago;

    public HorariosViewModel(String lab, String periodo, boolean vago) {
        this.lab = lab;
        this.periodo = periodo;
        this.vago = vago;
    }

    public HorariosViewModel(Horario horarios) {
        this.lab = horarios.getNomeLab();
        this.periodo=horarios.getPeriodo();
        this.vago = ("vaga".equals(horarios.getNomeAula()));
    }

    public String getLab() {
        return lab;
    }

    public void setLab(String lab) {
        this.lab = lab;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public boolean isVago() {
        return vago;
    }

    public void setVago(boolean vago) {
        this.vago = vago;
    }
    
    
}
