package br.com.horarios.horariosunipam.restClient;

import br.com.horarios.horariosunipam.restEntities.Horario;
import br.com.horarios.horariosunipam.restEntities.Laboratorio;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;


public class HorariosClient implements Serializable{
    private final String URL_CONEXAO_ADMINHORARIOS ="http://localhost:8080/infolabadminmvn/webresources/labs";
    private Client client;
    private String json;


    

    @PostConstruct
    public void init() {
       client = new Client();      
    }
    public List<Laboratorio> getLabs(){
        json= client.resource(URL_CONEXAO_ADMINHORARIOS+"/labs").get(String.class);
        Type typeStringArray = new TypeToken<ArrayList<Laboratorio>>(){}.getType();
        List<Laboratorio> labs= new Gson().fromJson(json, typeStringArray);
        return labs;
    }
    
    public List<Horario> getHorarios(){
        json= client.resource(URL_CONEXAO_ADMINHORARIOS+"/hoje").get(String.class);
        return getLabsFromJson();
    }

     public List<Horario> getHorariosDia(Integer diaSelecionado) {
        json= client.resource((URL_CONEXAO_ADMINHORARIOS+"/dia/"+diaSelecionado)).get(String.class);
        return getLabsFromJson();
    }

    public List<Horario> getHorariosLab(Integer labSelecionado) {
        json= client.resource(URL_CONEXAO_ADMINHORARIOS+ "/lab/"+labSelecionado).get(String.class);
        return getLabsFromJson();
    }

    public List<Horario> getHorarios(int labSelecionado, Integer diaSelecionado) {
        json= client.resource(URL_CONEXAO_ADMINHORARIOS+"/dia/"+diaSelecionado+"/"+labSelecionado).get(String.class);
        return getLabsFromJson();  
    }
    
    
    private List<Horario> getLabsFromJson(){
        Type typeListaHorario = new TypeToken<ArrayList<Horario>>(){}.getType();
        List<Horario> listaHorarios = new Gson().fromJson(json, typeListaHorario);
        return listaHorarios;
    }
    
    
    
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

   
    
    
}
